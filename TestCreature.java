public class TestCreature {

	public static void main(String[] args) {
		
		final int THING_COUNT = 4; //Tiger array size
		
		//Thing Section
		Thing[] things = new Thing[THING_COUNT];
		things[0] = new Tiger("Jan the");
		things[1] = new Thing("Banana");
		things[2] = new Thing("Phone");
		things[3] = new Fly("Jimmy the");
		
		System.out.println("\nThings: ");
		
		for(int i = 0; i < THING_COUNT; i++) {
			System.out.println(things[i]);
		}
		
		//Creature Section
		Creature[] creatures = new Creature[THING_COUNT];
		Tiger tiger = new Tiger("Tony The");
		Ant ant = new Ant("Bob The");
		Fly fly = new Fly("Sally The");
		Bat bat = new Bat("Anne The");
		creatures[0] = tiger;
		creatures[1] = ant;
		creatures[2] = fly;
		creatures[3] = bat;
		
		System.out.println("\nCreatures: ");
		
		for(int i = 0; i < THING_COUNT; i++) {
			System.out.println(creatures[i]);
			creatures[i].move(); //demonstrate polymorphism
		}
		
		System.out.println("\nTesting Eat Methods: ");
		tiger.eat(ant); //Tiger will eat anything
		tiger.whatDidYouEat();
		bat.eat(tiger);
		bat.eat(fly);
		bat.eat(things[1]);
		fly.eat(things[1]);
		fly.eat(things[3]);
		fly.whatDidYouEat();
		ant.eat(fly);
		ant.eat(tiger);
		ant.whatDidYouEat();
		
		

	}
}
