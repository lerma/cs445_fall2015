import static org.junit.Assert.*;
import org.junit.Test;


public class ThingTest {

	private Thing thing;
	private Ant ant;
	private Bat bat;
	private Fly fly;
	private Tiger tiger;
	
	public ThingTest() {
		thing = new Thing("Banana");
		ant = new Ant("Sam the");
		bat = new Bat("Jane the");
		fly = new Fly("Anne the");
		tiger = new Tiger("Tony the");
	}
	
	@Test
	public void testToString() {
		
		assertEquals(thing.toString(), "Banana" );
		assertEquals(ant.toString(), "Sam the Ant");
		assertEquals(bat.toString(), "Jane the Bat");
		assertEquals(fly.toString(), "Anne the Fly");
		assertEquals(tiger.toString(), "Tony the Tiger");
	}
	
	@Test
	public void testEat() {
		
		ant.eat(bat);
		assertEquals(ant.getStomach(), bat);
		bat.eat(fly);
		assertEquals(bat.getStomach(), fly);
		bat.eat(null); //if the bat eats nothing, stomach should stay as fly
		assertEquals(bat.getStomach(), fly);
		bat.eat(thing);
		assertEquals(bat.getStomach(), fly); //bat can't eat Things, still fly
		fly.eat(thing);
		assertEquals(fly.getStomach(), thing);
		fly.eat(bat);
		assertEquals(fly.getStomach(), thing); //fly can't eat a bat, still thing
		tiger.eat(thing); //tigers eat everything
		assertEquals(tiger.getStomach(), thing);
		tiger.eat(bat);
		assertEquals(tiger.getStomach(), bat);
		
	}

 	

}
