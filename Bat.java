public class Bat extends Creature implements Flyer {

	
	public Bat(String name) {
		super(name);
	}
	
	public void eat(Thing aThing) {
		
		if(aThing == null)
			return;
		
		if (aThing.getClass().getSimpleName().equals("Thing")) {
			System.out.println(this + " won't eat a " + aThing);
		} else {
			super.eat(aThing);
		}	
	}
	
	public void fly() {
		System.out.println(this + " is swooping through the dark.");
	}

	public void move() {
		fly();
	}
	
}
