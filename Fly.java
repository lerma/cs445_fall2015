
public class Fly extends Creature implements Flyer {

	
	public Fly(String name) {
		super(name);
	}
	

	public void eat(Thing aThing) {
		
		if(aThing.getClass().getSimpleName().equals("Thing")) {
			super.eat(aThing);
		} else  {
			System.out.println(this + " won't eat a " + aThing);
		}
			
		
	}
	
	public void fly() {
		
		System.out.println(this + " is buzzing around in flight.");
	}
	
	public void move() {
		fly();
	}
	
	
	
	
}
